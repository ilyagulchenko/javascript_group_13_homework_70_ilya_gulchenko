import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { numberValidator } from '../validate-number.directive';
import { FormService } from '../shared/form.service';
import { Form } from '../shared/form.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  registerForm!: FormGroup;
  isClick:boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formService: FormService
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      name: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      number: new FormControl('', [Validators.required, numberValidator]),
      workplace: new FormControl('', Validators.required),
      tShirt: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comments: new FormControl('', Validators.required),
      skills: new FormArray([]),
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.registerForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  addSkill() {
    const skills = <FormArray>this.registerForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required)
    });
    skills.push(skillGroup);
    this.isClick = false;
  }

  getSkillControls() {
    const skills = <FormArray>this.registerForm.get('skills');
    return skills.controls;
  }

  saveRegistration() {
    const id = Math.random().toString();
    const form = new Form(
      id,
      this.registerForm.value.comments,
      this.registerForm.value.name,
      this.registerForm.value.number,
      this.registerForm.value.patronymic,
      this.registerForm.value.size,
      this.registerForm.value.skills,
      this.registerForm.value.surname,
      this.registerForm.value.tShirt,
      this.registerForm.value.workplace,
    );

      this.formService.addRegisterForm(form).subscribe(void this.router.navigate(['success'], {relativeTo: this.route}));
  }
}
