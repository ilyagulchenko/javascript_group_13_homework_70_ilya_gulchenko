import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { SuccessRegisterComponent } from './success-register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidateNumberDirective } from './validate-number.directive';
import { FormItemComponent } from './form-item/form-item.component';
import { FormsComponent } from './forms/forms.component';
import { FormService } from './shared/form.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    RegisterFormComponent,
    SuccessRegisterComponent,
    ValidateNumberDirective,
    FormItemComponent,
    FormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
