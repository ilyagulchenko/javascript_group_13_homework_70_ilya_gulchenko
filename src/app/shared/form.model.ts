export class Form {
  constructor(
    public id: string,
    public comments: string,
    public name: string,
    public number: string,
    public patronymic: string,
    public size: string,
    public skills: any,
    public surname: string,
    public tShirt: string,
    public workplace: string,
  ) {}
}
