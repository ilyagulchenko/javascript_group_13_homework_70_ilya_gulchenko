import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';
import { Form } from './form.model';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class FormService {
  formsFetch = new Subject<Form[]>();

  constructor(private http: HttpClient) { }

  private forms: Form[] = [];

  addRegisterForm(form: Form) {
    const body = {
       comments: form.comments,
       name: form.name,
       number: form.number,
       patronymic: form.patronymic,
       size: form.size,
       skills: form.skills,
       surname: form.surname,
       tShirt: form.tShirt,
       workplace: form.workplace,
    };

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/forms.json', body);
  }

  fetchRegisterForms() {
    this.http.get<{[id: string]: Form}>('https://plovo-cc061-default-rtdb.firebaseio.com/forms.json').pipe(
      map(result => {
        return Object.keys(result).map(id => {
          const formData = result[id];

          return new Form(
            id,
            formData.comments,
            formData.name,
            formData.number,
            formData.patronymic,
            formData.size,
            formData.skills,
            formData.surname,
            formData.tShirt,
            formData.workplace
          );
        });
      })
    ).subscribe(forms => {
      this.forms = forms;
      this.formsFetch.next(this.forms.slice());
    })
  }

  removeRegisterForm(id: string) {
    return this.http.delete(`https://plovo-cc061-default-rtdb.firebaseio.com/forms/${id}.json`);
  }

}
