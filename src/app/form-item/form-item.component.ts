import { Component, Input } from '@angular/core';
import { Form } from '../shared/form.model';
import { FormService } from '../shared/form.service';

@Component({
  selector: 'app-form-item',
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.css']
})
export class FormItemComponent {
  @Input() form!: Form;

  constructor(private formService: FormService) { }

  onRemove() {
    this.formService.removeRegisterForm(this.form.id).subscribe(() => {
      this.formService.fetchRegisterForms();
    });
  }

}
