import { Component } from '@angular/core';

@Component({
  selector: 'app-registered',
  template: '<h1>May the force be with you...</h1>',
  styles: [`
    h1 {
      color: white;
      text-align: center;
      font-size: 100px;
      margin-top: 20%;
    }
  `]
})
export class SuccessRegisterComponent {}
