import { Component, OnDestroy, OnInit } from '@angular/core';
import { Form } from '../shared/form.model';
import { Subscription } from 'rxjs';
import { FormService } from '../shared/form.service';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit, OnDestroy {
  forms!: Form[];
  formsChangeSubscription!: Subscription;

  constructor(private formService: FormService) { }

  ngOnInit(): void {
    this.formsChangeSubscription = this.formService.formsFetch.subscribe((forms: Form[]) => {
      this.forms = forms;
    });
    this.formService.fetchRegisterForms();
  }

  ngOnDestroy(): void {
    this.formsChangeSubscription.unsubscribe();
  }

}
