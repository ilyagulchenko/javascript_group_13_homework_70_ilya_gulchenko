import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterFormComponent } from './register-form/register-form.component';
import { SuccessRegisterComponent } from './success-register.component';
import { FormsComponent } from './forms/forms.component';

const routes: Routes = [
  {path: '', component: RegisterFormComponent},
  {path: 'success', component: SuccessRegisterComponent},
  {path: 'forms', component: FormsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
